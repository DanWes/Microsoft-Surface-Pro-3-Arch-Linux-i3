Microsoft Surface Pro 3 with Arch Linux and i3
==============================================

(Dual) Booting Arch Linux (and Windows) with UEFI and Secure Boot
-----------------------------------------------------------------

- Save all your files, will will format the whole file system.
- Download Arch Linux ISO [archlinux-yyyy.mm.dd-dual.iso] [arch] via HTTP Direct
Downloads (choose a Mirror) or BitTorrent Download
[arch]: https://www.archlinux.org/download/ "Arch Linux Downloads"

- Create UEFI bootable USB device (with [Rufus] [rufus] and default settings)
[rufus]: https://rufus.akeo.ie/ "Rufus"

- Disable Secure Boot [\[Source: Windows\]] [win:sb]
  1. Shut down your Surface.
  2. Press and hold the volume-up button on your Surface and at the same time,
  press and release the power button.
  3. When you see the Surface logo, release the volume-up button.
The UEFI menu will display within a few seconds.
  4. Disable *Secure Boot Control*
[win:sb]: https://www.microsoft.com/surface/en-us/support/warranty-service-and-recovery/how-to-use-the-bios-uefi?os=windows-10&=undefined "Windows"

- (Optionally: Disable Windows Fast Boot &mdash; to switch between operating
  systems)


# Installing the Linux distro
Insert the live USB and boot by holding volume down, then the power button.

Optionally set the keyboard layout (default is US)

    $ loadkeys de-latin1

Set or in this case increase the font size

    $ setfont latarcyrheb-sun32

Connect to the Internet

    $ iw dev                # list wireless devices (Interface)
    $ iw dev wlp1s0 link    # Print information about the current link
    $ wifi-menu -o wlp1s0   # -o hides the password

Update the system clock

    $ timedatectl set-ntp true
    $ timedatectl status
    $ timedatectl set-timezone Europe/Berlin


## Partitioning
Note: Using `parted` with `mkpart` defines partitions via a *from* and a *to*
argument.

    $ lsblk                                         # list devices /dev/sda?
    $ parted /dev/sdx                               # could be /dev/sda
    (parted) mklabel gpt                            # for UEFI
    (parted) mkpart ESP fat32 1MiB 513MiB           # EFI System Partition
    (parted) set 1 boot on
    (parted) mkpart primary linux-swap 513MiB 9GiB  # for 8GB DRAM
    (parted) mkpart primary ext4 9GiB 40%           # Linux partition
    (parted) mkpart primary ntfs 40% 60%            # Shared partition
    (parted) quit

That last entry is for a shared NTFS filesystem that both operating systems
(Linux and Windows) can use, e. g. for a cloud service like Dropbox, email,
downloads, etc. The Windows section is left unallocated, it will be formatted by
the windows installer.


## Formatting

    $ mkfs.fat -F32 /dev/sdx1            # UEFI boot must be fat32
    $ mkswap /dev/sdx2
    $ swapon /dev/sdx2
    $ mkfs.ext4 /dev/sdx3                # Linux root
    $ mkfs.ntfs -f /dev/sdx4


## Mount
Mount the root partition on `/mnt` and for the boot partition first create
directory and then mount it also.

    $ mount /dev/sdx3 /mnt
    $ mkdir -p /mnt/boot
    $ mount /dev/sbx1 /mnt/boot


## Install the base packages and configure the system
Edit `/etc/pacman.d/mirrorlist` and select a download mirror (uncommenting the
specific line). [\[see also Arch Linux Wiki: Mirrors\]] [arch:mirrors]
[arch:mirrors]: https://wiki.archlinux.org/index.php/Mirrors "Arch Linux Mirrors"

Install the base packages or more

    $ pacstrap /mnt base
    # or
    $ pacstrap -i /mnt base base-devel btrfs-progs
    # and for wifi
    $ pacstrap /mnt dialog wpa_suplicant

Generate an `fstab` file, so the filesystem knows how to mount the disk
partitions. [\[see also Arch Linux Wiki: fstab\]] [arch:fstab]
[arch:fstab]: https://wiki.archlinux.org/index.php/Fstab "Arch Linux fstab"

    $ genfstab -U /mnt >> /mnt/etc/fstab

Change root into the new system

    $ arch-chroot /mnt /bin/bash    # bash shell instead of the default sh

For localizations uncomment `en_US.UTF-8 UTF-8` and other needed localizations
in `/etc/locale.gen`, e. g. `de_DE.UTF-8 UTF-8`. Finally generate the locale
files:

    $ locale-gen

Set the LANG variable in `/etc/locale.conf` accordingly

    LANG=en_US.UTF-8

Optionally set the keyboard layout and set the font in `/etc/vconsole.conf`

    KEYMAP=de-latin1
    FONT=latarcyrheb-sun32

Set the time zone, e. g. for Germany as Europe/Berlin.
Three ways possible: [\[see also Arch Linux Wiki: Time zone\]] [arch:tz]
[arch:tz]: https://wiki.archlinux.org/index.php/Fstab "Arch Linux Time zone"

- interactively with `tzselect` (e. g. 7) Europe, 16) Germany, 2) Germany, 1) yes),
- via `timedatectl`

        $ timedatectl                             # check the current zone
        $ timedatectl list-timezones              # list available zones
        $ timedatectl set-timezone Europe/Berlin  # change your time zone

- or create the symlink manually

        $ ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

Set the Hardware Clock from the System Clock, and update the timestamps in
`/etc/adjtime` via:

    hwclock --systohc --utc

Create an entry for your hostname in `/etc/hostname` (witch is a computer name):

    surfacepro3name

A matching entry in `/etc/hosts` is recommended

    127.0.0.1       localhost.localdomain   localhost       surfacepro3
    ::1             localhost.localdomain   localhost       surfacepro3

Set the root password:

    $ passwd

### Bootloader
Install the bootloader.

    $ pacman -S efibootmgr efitools
    $ bootctl --path=/boot install

Custom entries for the Systemd-boot must be made for linux kernels.
Edit `/boot/loader/loader.conf`

    default arch
    timeout 4
    editor  0

Install the intel microcode updater and note the `PARTUUID` for the root `/`
partition on your Arch drive `/dev/sdx3` via `blkid`

    $ pacman -S intel-ucode
    $ blkid

Add the boot entry for the linux kernel in `/boot/loader/entries/arch.conf` and
load the intel microcode updater before `initramfs`

    title   Arch
    linux   /vmlinuz-linux
    initrd  /intel-ucode.img
    initrd  /initramfs-linux.img
    options root=PARTUUID=THE-NUMBER-GIVEN-BY-BLKID rw

Download a Microsoft-signed loader and hashing tool, to generate compatible keys
for any kernel, tool, etc. Also, change (copy) the name of the standard systemd
bootloader so that the PreLoader will recognize it

    $ cd /boot/EFI/systemd
    $ pacman -S wget
    $ wget http://blog.hansenpartnership.com/wp-uploads/2013/PreLoader.efi
    $ wget http://blog.hansenpartnership.com/wp-uploads/2013/HashTool.efi
    $ cp /boot/EFI/systemd/systemd-bootx64.efi /boot/EFI/systemd/loader.efi

The boot order on the motherboard NVRAM has to be changed so that PreLoader.efi
and HashTool.efi are present

    $ efibootmgr -c -d /dev/sdx -p 1 -L PreLoader -l /EFI/systemd/PreLoader.efi
    $ efibootmgr -c -d /dev/sdx -p 1 -L HashTool -l /EFI/systemd/HashTool.efi

and in the correct order; PreLoader, HashTool and Linux Boot Manager.

    $ efibootmgr -v                     # get the numbers 000x
    $ efibootmgr -o 000P,000H,000L      # in my case it was 0003,0004,0000

Exit the root shell

    $ exit

Unmount all the partitions

    $ umount -R /mnt

Restart / shutdown the machine and unplug the live USB.

    $ shutdown now

Turn secure boot back on. After reboot you should be loaded into a blue screen.
The screen prompts you that no signed binaries were found and that you need to
sign them (Enroll). Using the HashTool the following items MUST be signed for
the Linux kernel to boot correctly:

    /boot/EFI/systemd/loader.efi
    /boot/vmlinuz-linux

You can navigate back through folders using the ../ option. If you forgot to
sign something and can not boot, don’t worry. You can load up the live USB
(disable Secure Boot), and set HashTool.efi to be the first boot entry
using `efibootmgr`. Then, you can re-sign (enroll) as needed.



# Installing Windows
Install windows normally.

Before you restart for the first time disable fast startup (Windows 10)
1. Type Control Panel in the search box.
2. Click Control Panel.
3. Click Power Options.
4. Click Choose what the power buttons do.
5. Click Change settings that are currently unavailable.
6. Scroll down to Shutdown settings and uncheck Turn on fast startup.
7. Click Save changes.

To make Systemd-boot able to load windows through PreLoader.efi the Windows boot
manager must be on the same ESP. But Windows will see the ESP and install it’s
manager to `/boot/EFI/Microsoft/`.

Windows will overwrite your existing boot order in NVRAM to place its boot
manager first. Load up the live USB again (disable Secure Boot) and change the
boot order back to PreLoader.efi being first:

    $ mount /dev/sdx3 /mnt               # the Arch / partition
    $ mount /dev/sdx1 /mnt/boot          # The Arch /boot partition
    $ arch-chroot /mnt /bin/bash
    $ efibootmgr -v                      # get the numbers 000x
    $ efibootmgr -o 000P,000H,000L,000W
    $ exit
    $ umount -R /mnt
    $ reboot

To make an entry in the systemd-boot menu as default select the entry and press
`d`.
It could be handy to make Windows the default entry because of automatic updates
that require multiple restarts, etc.
